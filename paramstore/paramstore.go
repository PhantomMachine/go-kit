package paramstore

import (
	"fmt"
	"os"
	"reflect"
	"strconv"
	"strings"
	"time"
)

type config struct {
	Env string
}

func Load(spec interface{}) error {

	cfg := config{
		Env: "",
	}

	infos, err := parseSpec("/"+cfg.Env, spec)
	if err != nil {
		return err
	}

	for _, info := range infos {
		var (
			value string
			ok    bool
		)
		if info.EnvKey != "" {
			value, ok = os.LookupEnv(info.EnvKey)
		}
		if !ok {
			if info.Default == "" && info.Required {
				return fmt.Errorf("param required: %s", info.Name)
			}

			value = info.Default
		}

		err = setValue(value, info.Field)
		if err != nil {
			return err
		}
	}

	return nil
}

type fieldInfo struct {
	Name     string        `json:"name"`
	EnvKey   string        `json:"env_key"`
	Default  string        `json:"default"`
	Required bool          `json:"required"`
	Field    reflect.Value `json:"-"`
}

func parseSpec(pathPrefix string, spec interface{}) ([]fieldInfo, error) {
	specVal := reflect.ValueOf(spec)
	if specVal.Kind() != reflect.Ptr {
		return nil, fmt.Errorf("specification must be a struct pointer")
	}

	specElem := specVal.Elem()
	if specElem.Kind() != reflect.Struct {
		return nil, fmt.Errorf("specification must be a struct pointer")
	}

	specType := specElem.Type()

	var infos []fieldInfo

	for i := 0; i < specElem.NumField(); i++ {
		field := specElem.Field(i)

		if !field.CanSet() {
			continue
		}

		fieldType := specType.Field(i)

		envKey := strings.ToUpper(fieldType.Tag.Get("env"))

		if envKey == "-" {
			envKey = ""
		}

		if envKey == "" {
			continue
		}

		isTrue := func(s string) bool {
			b, _ := strconv.ParseBool(s)
			return b
		}

		defaultTagValue, defaultTagExists := fieldType.Tag.Lookup("default")
		requiredTagValue, requiredTagExists := fieldType.Tag.Lookup("required")
		required := isTrue(requiredTagValue)

		if !requiredTagExists && !defaultTagExists {
			required = true
		}

		info := fieldInfo{
			Name:     fieldType.Name,
			EnvKey:   envKey,
			Default:  defaultTagValue,
			Required: required,
			Field:    field,
		}

		infos = append(infos, info)
	}

	return infos, nil
}

func setValue(value string, field reflect.Value) error {
	fieldType := field.Type()

	{
		decoder := decoderFrom(field)
		if decoder != nil {
			return decoder.Decode(value)
		}

		// if t := textUnmarshaler(field); t != nil {
		// 	return t.UnmarshalText([]byte(value))
		// }

		// if b := binaryUnmarshaler(field); b != nil {

		// }
	}

	if fieldType.Kind() == reflect.Ptr {
		fieldType = fieldType.Elem()
		field = field.Elem()
	}

	switch fieldType.Kind() {
	case reflect.String:
		field.SetString(value)
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		var (
			val int64
			err error
		)

		if field.Kind() == reflect.Int64 && fieldType.PkgPath() == "time" && fieldType.Name() == "Duration" {
			var d time.Duration
			d, err = time.ParseDuration(value)
			val = int64(d)
		} else {
			val, err = strconv.ParseInt(value, 0, fieldType.Bits())
		}

		if err != nil {
			return err
		}

		field.SetInt(val)
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		val, err := strconv.ParseUint(value, 0, fieldType.Bits())
		if err != nil {
			return err
		}

		field.SetUint(val)
	case reflect.Float32, reflect.Float64:
		val, err := strconv.ParseFloat(value, fieldType.Bits())
		if err != nil {
			return err
		}

		field.SetFloat(val)
	case reflect.Bool:
		val, err := strconv.ParseBool(value)
		if err != nil {
			return err
		}

		field.SetBool(val)
	}

	return nil
}

type Decoder interface {
	Decode(value string) error
}

func decoderFrom(field reflect.Value) Decoder {
	var d Decoder

	interfaceFrom(field, func(v interface{}, ok *bool) {
		d, *ok = v.(Decoder)
	})

	return d
}

func interfaceFrom(field reflect.Value, fn func(interface{}, *bool)) {
	if !field.CanSet() {
		return
	}

	var ok bool
	fn(field.Interface(), &ok)

	if !ok && field.CanAddr() {
		fn(field.Addr().Interface(), &ok)
	}
}
