package logging

import (
	"net"
	"net/http"
	"strings"

	"go.uber.org/zap"
)

//
// BEGIN: PrivateRemoteAddressFromRequest helpers
//

// https://stackoverflow.com/questions/41240761/check-if-ip-address-is-in-private-network-space

var privateNoLoopbackIpBlocks []*net.IPNet

func init() {

	privateNoLoopbackCidrs := []string{
		"10.0.0.0/8",     // RFC1918
		"172.16.0.0/12",  // RFC1918
		"192.168.0.0/16", // RFC1918
		"169.254.0.0/16", // RFC3927 link-local
		"fe80::/10",      // IPv6 link-local
		"fc00::/7",       // IPv6 unique local addr
	}

	privateNoLoopbackIpBlocks = make([]*net.IPNet, 0, len(privateNoLoopbackCidrs))

	for _, cidr := range privateNoLoopbackCidrs {
		_, block, err := net.ParseCIDR(cidr)
		if err != nil {
			zap.S().Fatalw(
				"failed to parse private cidr",
				"cidr", cidr,
				"error", err,
			)
		}

		privateNoLoopbackIpBlocks = append(privateNoLoopbackIpBlocks, block)
	}
}

// isPrivateIp returns if an IP is private and if it is a loop address
func isPrivateIp(ip net.IP) (bool, bool) {

	if ip.IsLoopback() {
		return true, true
	}

	if ip.IsLinkLocalUnicast() || ip.IsLinkLocalMulticast() {
		return true, false
	}

	for _, ipBlock := range privateNoLoopbackIpBlocks {
		if ipBlock.Contains(ip) {
			return true, false
		}
	}

	return false, false
}

// GetPrivateRemoteAddress attempts to identify the origin
// IP address of a request within the sphere of a private network.
//
// For privacy reasons we do not want to log our user's public IP
// address.
//
// This is done by iterating through the requests' chain of IP
// addresses and returning the last address in the private cidr
// range before the first public address.
//
// This traversed segment of the request chain is also known as
// the "private network trace".
//
// If there is a mix of loopback and non-loopback addresses in the
// "private network trace" then the last non-loopback address will
// be returned.
func GetPrivateRemoteAddress(r *http.Request) string {

	privIp, isLoopback := privateIpFromHostPort(r.RemoteAddr)
	if privIp != "" {
		var candidateIp string
		var candidateIsLoopback bool

		// search X-Forwarded-For header for possible ips closer to the remote origin
		proxyIps := strings.Split(r.Header.Get("X-Forwarded-For"), ",")

		for i := len(proxyIps) - 1; i >= 0; i-- {

			candidateIp = strings.TrimSpace(proxyIps[i])
			if candidateIp == "" {
				break
			}

			candidateIp, candidateIsLoopback = isStrPrivateIp(candidateIp)
			if candidateIp == "" {
				break
			}

			// if a non-loopback ip was found earlier, don't return a loopback ip
			// instead keep searching for a non-loopback ip
			if !isLoopback && candidateIsLoopback {
				continue
			}

			privIp = candidateIp
			isLoopback = candidateIsLoopback
		}
	}

	return privIp
}

func privateIpFromHostPort(s string) (string, bool) {

	host, _, err := net.SplitHostPort(s)
	if err != nil {
		return "", false
	}

	host = strings.TrimSpace(host)

	return isStrPrivateIp(host)
}

func isStrPrivateIp(s string) (string, bool) {
	var ip net.IP

	err := ip.UnmarshalText([]byte(s))
	if err != nil {
		return "", false
	}

	ok, isLoopback := isPrivateIp(ip)
	if !ok {
		return "", false
	}

	return ip.String(), isLoopback
}
