package logging

import (
	"context"
	"net/http"
	"time"

	"go.opentelemetry.io/otel/trace"
)

type ctxHttpRequestStart struct{}

var MiddlewareHttpRequestStart ctxHttpRequestStart = struct{}{}

func (k ctxHttpRequestStart) Set() func(http.HandlerFunc) http.HandlerFunc {

	return func(next http.HandlerFunc) http.HandlerFunc {

		return func(w http.ResponseWriter, r *http.Request) {

			newCtx := context.WithValue(r.Context(), k, time.Now().UTC())
			newReq := r.WithContext(newCtx)

			next(w, newReq)
		}
	}
}

func (k ctxHttpRequestStart) Get(ctx context.Context) time.Time {

	t, ok := ctx.Value(k).(time.Time)
	if ok {
		return t
	}

	return time.Time{}
}

func newCtxLogger(ctx context.Context) logger {
	logger := Logger

	spanCtx := trace.SpanContextFromContext(ctx)
	logger = logger.With(
		"otel_trace_id", spanCtx.TraceID(),
		"otel_span_id", spanCtx.SpanID(),
	)

	if startTime := MiddlewareHttpRequestStart.Get(ctx); !startTime.IsZero() {
		logger = logger.With(
			"http_request_start_at", startTime.Format(time.RFC3339Nano),
		)
	}

	return logger
}
