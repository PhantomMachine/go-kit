package logging

import "net/http"

func getRequestArgs(r *http.Request) []interface{} {
	return []interface{}{
		"http.useragent", r.UserAgent(),
		// "http.private_remote_address", GetPrivateRemoteAddress(r), // TODO
		"http.method", r.Method,
		"http.url", r.URL.String(),
	}
}

func WithRequest(r *http.Request) logger {
	return newCtxLogger(r.Context()).With(getRequestArgs(r)...)
}
