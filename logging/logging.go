package logging

import (
	"log"
	"time"

	_ "github.com/jsternberg/zap-logfmt"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

// type logImpl struct {
// }

type logger = *zap.SugaredLogger

// type logger interface {
// 	Infow(msg string, in ...interface{})
// 	Warnw(msg string, in ...interface{})
// 	Errorw(msg string, in ...interface{})
// 	Fatalw(msg string, in ...interface{})

// 	With(args ...interface{}) *zap.SugaredLogger // note(andrew): I think, b/c of the return here, that *zap.SugaredLogger breaks the interface I want of `With(args) logger`
// }

type Level = zapcore.Level

const (
	DebugLevel Level = iota - 1
	InfoLevel
	WarnLevel
	ErrorLevel
	DPanicLevel
	PanicLevel
	FatalLevel

	// _minLevel = DebugLevel
	// _maxLevel = FatalLevel
)

func init() {
	if err := SetLevel(zap.DebugLevel); err != nil {
		log.Fatal("failed to initialize zap logger: ", err.Error())
	}
}

func SetLevel(level Level) error {
	lgr, err := newLogger(level)
	if err != nil {
		return err
	}

	zap.ReplaceGlobals(lgr)

	Logger = lgr.Sugar()

	return nil
}

func newLogger(zapLevel zapcore.Level) (*zap.Logger, error) {
	cfg := zap.NewProductionConfig()
	cfg.Level = zap.NewAtomicLevelAt(zapLevel)
	cfg.Sampling = nil
	cfg.EncoderConfig.EncodeTime = zapcore.TimeEncoder(func(t time.Time, enc zapcore.PrimitiveArrayEncoder) {

		// exactly the same as zapcore.RFC3339NanoTimeEncoder but using UTC localization
		zapcore.RFC3339NanoTimeEncoder(t.UTC(), enc)
	})
	cfg.EncoderConfig.EncodeDuration = zapcore.StringDurationEncoder
	cfg.DisableStacktrace = true

	cfg.Encoding = "logfmt"

	zapOptions := []zap.Option{}
	return cfg.Build(zapOptions...)
}

// func (*logImpl) Infow(msg string, in ...interface{}) {

// }

// func (*logImpl) Warnw(msg string, in ...interface{}) {

// }

// func (*logImpl) Errorw(msg string, in ...interface{}) {

// }

// func (*logImpl) Fatalw(msg string, in ...interface{}) {

// }

// func (*logImpl) With(args ...interface{}) *zap.SugaredLogger {
// 	return &logImpl{}
// }

var Logger logger
