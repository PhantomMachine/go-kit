package response

import (
	"context"
	"strconv"

	"go.uber.org/zap"
)

type ContextLoggerFunc func(context.Context) *zap.SugaredLogger

func DefaultContextLogger(context.Context) *zap.SugaredLogger {
	return zap.S()
}

type ErrorResponseLogHandlerFunc func(ctx context.Context, logger *zap.SugaredLogger, statusCode int, errCode, developerMessage string, internalErr error)

// DefaultErrorResponseLogHandler requires logger to be non-nil
func DefaultErrorResponseLogHandler(ctx context.Context, logger *zap.SugaredLogger, statusCode int, errCode, developerMessage string, internalErr error) {

	logger = logger.With(
		"error_response", loggableErrResponse{
			InternalError:    internalErr,
			StatusCode:       statusCode,
			ErrCode:          errCode,
			DeveloperMessage: developerMessage,
		},
	)

	if internalErr == context.Canceled {
		// means the service may have taken too long to respond and the client
		// is closing its connection or the client may have been disconnected
		// due to the internet of things
		//
		// TODO: may want to modify such that we log this error at a higher level
		// if a "considerable" amount of time has passed between the request
		// start time and now
		//
		// otherwise APM / loadbalancing statistics can reveal latency woes more
		// reliably
		msg := internalErr.Error()
		logger.Debugw(msg)
		return
	}

	if internalErr == context.DeadlineExceeded {
		// means we are going to circuit-break the request because something
		// either in this service or upstream is taking too long to respond
		msg := internalErr.Error()
		logger.Warnw(msg)
		return
	}

	logger = logger.With(
		"error.kind", strconv.Itoa(statusCode),
	)

	if statusCode >= 500 {
		if s := GetVerboseStackTrace(internalErr); s != "" {
			logger = logger.With(
				"error.stack", s,
			)
		}
	}

	// specific status code log statements
	switch statusCode {
	case 404:
		logger.Debugw("404 error response")
	case 500:
		logger.Errorw("500 error response")
	default:

		// range based status code log statements

		if statusCode < 400 {
			logger.Errorw("abnormal(<400) error response")
		} else if statusCode < 500 {
			logger.Warnw("4xx error response")
		} else if statusCode < 600 {
			logger.Errorw("5xx error response")
		} else {
			logger.Errorw("abnormal(>=600) error response")
		}
	}
}

type responseFactory struct {
	contextLogger           ContextLoggerFunc
	errorResponseLogHandler ErrorResponseLogHandlerFunc
}

func NewResponseFactory() *responseFactory {
	return &responseFactory{
		contextLogger:           DefaultContextLogger,
		errorResponseLogHandler: DefaultErrorResponseLogHandler,
	}
}

var defaultResponseFactory = NewResponseFactory()

func GetDefaultResponseFactory() *responseFactory {
	return defaultResponseFactory
}

const (
	responseTypeValid responseType = iota
	responseTypeError
)

// New uses the defaultResponseFactory to create
// a new high level response object.
func (rf *responseFactory) New(options ...Option) Response {

	r := Response{
		respFactory:  rf,
		responseType: responseTypeValid,
	}

	for _, op := range options {
		op(&r)
	}

	return r
}
