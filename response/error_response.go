package response

import (
	"context"
	"fmt"
	"io"
	"net/http"
	"runtime/debug"
	"strconv"

	pkgerrors "github.com/pkg/errors"
	"go.uber.org/zap"
)

const ErrCodeInternalServerError = "internal-server-error"

func (f *responseFactory) NewErrResponse(statusCode int, errCode, developerMessage string) ErrResponse {

	return ErrResponse{
		respFactory: f,

		ErrResponseBody: ErrResponseBody{
			ErrCode:          errCode,
			DeveloperMessage: developerMessage,
		},
		StatusCode: statusCode,
	}
}

// NewInternalErrResponse will return a 500 error response
// if the error passed to it is NOT already an error response
//
// If a nil error is passed in a non-nil ErrResponse will still
// be returned. It is the caller's responsability to ensure they
// pass a non-nil error. This object is only useful for handling
// cases where an error has clearly occurred.
//
// if the err is an error response already you should probably
// avoid calling this function because it will make a copy
func (f *responseFactory) NewInternalErrResponse(err error) ErrResponse {

	if err == nil {
		err = NewStackTracedErr(string(debug.Stack()), err)
	}

	if t, ok := err.(ErrResponse); ok {
		return t
	}

	if err == context.Canceled {
		return ErrResponse{
			respFactory: f,

			ErrResponseBody: ErrResponseBody{
				ErrCode:          "context-canceled",
				DeveloperMessage: http.StatusText(http.StatusBadRequest),
			},
			StatusCode:    http.StatusBadRequest,
			InternalError: err,
		}
	}

	if err != context.DeadlineExceeded {
		if _, ok := err.(fmt.Formatter); !ok {
			err = pkgerrors.WithStack(err)
		}
	}

	return ErrResponse{
		respFactory: f,

		ErrResponseBody: ErrResponseBody{
			ErrCode:          ErrCodeInternalServerError,
			DeveloperMessage: http.StatusText(http.StatusInternalServerError),
		},
		StatusCode:    http.StatusInternalServerError,
		InternalError: err,
	}
}

type ErrResponseBody struct {
	Status           int    `json:"status"`
	ErrCode          string `json:"error_code"`
	DeveloperMessage string `json:"developer_message"`
}

// swagger:model
type ErrResponse struct {
	respFactory *responseFactory

	// in:body
	ErrResponseBody
	// swagger:ignore
	StatusCode int
	// swagger:ignore
	InternalError error
}

// NewErrResponse uses the default ResponseFactory to create a new error response
// with a particular response StatusCode, errCode body string and developerMessage body string
func NewErrResponse(statusCode int, errCode, developerMessage string) ErrResponse {

	return GetDefaultResponseFactory().NewErrResponse(statusCode, errCode, developerMessage)
}

// NewInternalErrResponse will return a 500 error response
// if the error passed to it is NOT already an error response
//
// If a nil error is passed in a non-nil ErrResponse will still
// be returned. It is the caller's responsability to ensure they
// pass a non-nil error. This object is only useful for handling
// cases where an error has clearly occurred.
//
// if the err is an error response already you should probably
// avoid calling this function because it will make a copy
//
// Uses the default ResponseFactory
func NewInternalErrResponse(err error) ErrResponse {

	return GetDefaultResponseFactory().NewInternalErrResponse(err)
}

func (r ErrResponse) Unwrap() error { return r.InternalError }

func (r ErrResponse) String() string {

	return strconv.Itoa(r.StatusCode) + ": " + r.ErrCode + ": " + r.DeveloperMessage
}

func (r ErrResponse) responseFactory() *responseFactory {

	if r.respFactory != nil {
		return r.respFactory
	}

	return GetDefaultResponseFactory()
}

func (r ErrResponse) Error() string {
	return r.String()
}

// Format defends against an ErrResponse getting wrapped in
//
// a pkg/errors.WithStack call
func (r ErrResponse) Format(s fmt.State, verb rune) {
	switch verb {
	case 's', 'v':
		_, err := io.WriteString(s, r.String())
		if err != nil {
			zap.S().Debugw(
				"response: ErrResponse: Format failed",
				"error", err,
			)
		}
	}
}

func (r ErrResponse) EqualsIgnoreCause(otherErrResponse error) bool {
	var other ErrResponse
	var ok bool

	if other, ok = otherErrResponse.(ErrResponse); !ok {
		return false
	}

	return (r.DeveloperMessage == other.DeveloperMessage) && (r.ErrCode == other.ErrCode) && (r.StatusCode == other.StatusCode)
}

// CausedBy returns a copy of the error response
// with InternalError set to the passed err value
func (r ErrResponse) CausedBy(err error) ErrResponse {

	if err == nil {
		return r
	}

	switch t := err.(type) {
	case ErrResponse:

		if r.EqualsIgnoreCause(t) {
			return t
		}

		if t.InternalError != nil {
			r.InternalError = t.InternalError
		} else {
			r.InternalError = err
		}
	default:
		r.InternalError = err
	}

	if r.InternalError != nil && r.InternalError != context.Canceled && r.InternalError != context.DeadlineExceeded {
		// if the error does not have a stack trace, add one
		if _, ok := r.InternalError.(fmt.Formatter); !ok {
			r.InternalError = pkgerrors.WithStack(r.InternalError)
		}
	}

	return r
}

func (r ErrResponse) GetBodyJson() ErrResponseBody {

	result := r.ErrResponseBody

	// legacy standards include the status in the response body
	// so shimming it in consistently

	result.Status = r.StatusCode

	if result.Status == 0 {
		result.Status = http.StatusInternalServerError
	}

	return result
}

func (r ErrResponse) Response() Response {

	return Response{
		respFactory: r.responseFactory(),

		responseType: responseTypeError,
		errResponse:  r,
		bodyType:     bodyTypeJson,
		body:         r.GetBodyJson(),
		StatusCode:   r.StatusCode,
	}
}

// WriteResponse is an alias for
// *.Response().WriteResponse(...)
func (r ErrResponse) WriteResponse(ctx context.Context, w http.ResponseWriter) {
	r.Response().WriteResponse(ctx, w)
}

// StaticHandlerFunc is an alias for
// *.Response().StaticHandlerFunc()
//
//
// description copied from Response.HanderFunc():
//
// StaticHandlerFunc will convert the response body to bytes
// and return a simple http.HandlerFunc
//
// If anything goes wrong converting the body to bytes
// then the function panics
//
// Useful when creating a static response handler
// and all the body fits into memory
func (r ErrResponse) StaticHandlerFunc() http.HandlerFunc {
	return r.Response().StaticHandlerFunc()
}

// HandlerFunc is an alias for
// *.Response().HandlerFunc()
//
// note that if the inputs are static and fit into memory
// you may prefer to use StaticHandlerFunc() instead
func (r ErrResponse) HandlerFunc() http.HandlerFunc {
	return r.Response().HandlerFunc()
}
