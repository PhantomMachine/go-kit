package response

import (
	"io"
	"net/http"
	"strconv"

	"go.uber.org/zap"
)

type Option func(*Response)

func BodyJson(json interface{}) Option {
	return func(r *Response) {
		r.bodyType = bodyTypeJson
		r.body = json
	}
}

func BodyBytes(bytes []byte) Option {
	return func(r *Response) {
		r.bodyType = bodyTypeBytes
		r.body = bytes
	}
}

func BodyReader(reader io.Reader) Option {
	return func(r *Response) {
		r.bodyType = bodyTypeReader
		r.body = reader
	}
}

func StatusCode(statusCode int) Option {
	return func(r *Response) {
		r.StatusCode = statusCode
	}
}

// Logger should only be used if you wish to set the logger
// for a specific response where the context has already
// been processed.
//
// If you wish to change a response's strategy for creating a
// logger for an arbitrary context use the ContextLogger option
// instead.
func Logger(logger *zap.SugaredLogger) Option {
	return func(r *Response) {
		r.Logger = logger
	}
}

func ContextLogger(f ContextLoggerFunc) Option {
	return func(r *Response) {
		r.ContextLogger = f
	}
}

func ErrorResponseLogHandler(f ErrorResponseLogHandlerFunc) Option {
	return func(r *Response) {
		r.ErrorResponseLogHandler = f
	}
}

// ContentLength is mainly only useful if rendering a static file
// so you can get the file size from the os and let the client read
// from it directly.
//
// Most microservices will not use this option as there are better ways
// to serve up file contents.
func ContentLength(contentLength int) Option {
	return func(r *Response) {
		r.NoAutomaticContentLengthHeader = true

		if r.Header == nil {
			r.Header = http.Header{}
		}

		r.Header.Set(HeaderKeyContentLength, strconv.Itoa(contentLength))
	}
}

// Header loads a http Header into the response.
//
// Note in general you should not mix this option with HeaderValue
// unless Header is called first and the argument to Header is
// not shared with other responses. If it is shared call `.Clone()`
// on it before passing it to this option!
func Header(header http.Header) Option {
	return func(r *Response) {

		r.Header = header
	}
}

func HeaderValue(k, v string) Option {
	return func(r *Response) {

		if r.Header == nil {
			r.Header = http.Header{}
		}

		r.Header.Set(k, v)
	}
}

func NoAutomaticContentLengthHeader(b bool) Option {
	return func(r *Response) {
		r.NoAutomaticContentLengthHeader = b
	}
}

func LoggerDisabled(b bool) Option {
	return func(r *Response) {
		r.LoggerDisabled = b
	}
}
