package response

import (
	"bytes"
	"context"
	"encoding"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"runtime/debug"
	"strconv"

	"go.uber.org/zap"
)

// globals needing initalization
var fallbackErrJsonResponse []byte
var fallbackErrJsonResponseContentLength string

// header key consts
const (
	HeaderKeyContentType   = "Content-Type"
	HeaderKeyContentLength = "Content-Length"
)

// header value consts
const (
	HeaderValueContentTypeJson = "application/json"
)

// technically consts that require being declared as vars
var nullJsonBody = []byte("null\n")
var contentLengthOfNullJsonBody = strconv.Itoa(len(nullJsonBody))

func GetVerboseStackTrace(tracedErr error) string {
	return tracedErr.Error() // TODO
}

func NewStackTracedErr(stack string, v interface{}) error {
	return fmt.Errorf("not yet defined") // TODO
}

func New(options ...Option) Response {
	return GetDefaultResponseFactory().New(options...)
}

type bodyType uint8

const (
	bodyTypeNone bodyType = iota
	bodyTypeJson
	bodyTypeBytes
	bodyTypeReader
)

type responseType uint8

type Response struct {
	respFactory *responseFactory

	ContextLogger           ContextLoggerFunc
	ErrorResponseLogHandler ErrorResponseLogHandlerFunc
	responseType            responseType
	errResponse             ErrResponse

	bodyType bodyType
	// body can be []byte, io.Reader, otherwise will json marshall if non-nil
	body interface{}

	Logger         *zap.SugaredLogger
	LoggerDisabled bool

	// NoAutomaticContentLengthHeader disables the implicit requirement to always
	// send back a Content-Length response header
	NoAutomaticContentLengthHeader bool
	//
	StatusCode int
	Header     http.Header
}

func canMarshalToJSON(v interface{}) bool {

	if _, ok := v.(json.Marshaler); ok {
		return true
	}

	if _, ok := v.(encoding.TextMarshaler); ok {
		return true
	}

	return false
}

// WriteResponse
//
// Note if the response was not created from a response factory then it will always be a 500 error
func (r Response) WriteResponse(ctx context.Context, w http.ResponseWriter) {
	var err error

	switch r.responseType {
	case responseTypeError:

		if r.StatusCode == 0 {
			r.StatusCode = http.StatusInternalServerError
		}

		r.logErrorResponse(ctx)
	case responseTypeValid:
		fallthrough
	default:

		if r.StatusCode == 0 {
			r.StatusCode = http.StatusOK
		}
	}

	header := w.Header()

	for k, values := range r.Header {
		for i := range values {
			header.Add(k, values[i])
		}
	}

	if ctx.Err() == context.Canceled {
		return
	}

	switch r.bodyType {
	case bodyTypeBytes:
		var contentLength int

		bytes, ok := r.body.([]byte)
		if ok {
			contentLength = len(bytes)
		}

		if !r.NoAutomaticContentLengthHeader && header.Get(HeaderKeyContentLength) == "" {
			header.Set(HeaderKeyContentLength, strconv.Itoa(contentLength))
		}

		w.WriteHeader(r.StatusCode)

		if contentLength == 0 {
			return
		}

		_, err = w.Write(bytes)
		if err != nil {
			if ctx.Err() == context.Canceled {
				return
			}

			r.getLogger(ctx).Debugw(
				"client failed to read all []byte content",
				"error", err,
			)
		}
	case bodyTypeReader:

		reader, ok := r.body.(io.Reader)
		if !ok {
			if !r.NoAutomaticContentLengthHeader && header.Get(HeaderKeyContentLength) == "" {
				header.Set(HeaderKeyContentLength, "0")
			}
			w.WriteHeader(r.StatusCode)
			return
		}

		if !r.NoAutomaticContentLengthHeader && header.Get(HeaderKeyContentLength) == "" {

			bytes, err := ioutil.ReadAll(reader)
			if err != nil {

				r.writeRenderError(ctx, w, err)
				return
			}

			header.Set(HeaderKeyContentLength, strconv.Itoa(len(bytes)))

			w.WriteHeader(r.StatusCode)

			_, err = w.Write(bytes)
			if err != nil {
				if ctx.Err() == context.Canceled {
					return
				}

				r.getLogger(ctx).Debugw(
					"client failed to read all buffered io.Reader content",
					"error", err,
				)
			}

			return
		}

		w.WriteHeader(r.StatusCode)

		_, err = io.Copy(w, reader)
		if err != nil {
			if ctx.Err() == context.Canceled {
				return
			}

			r.getLogger(ctx).Debugw(
				"client failed to read all io.Reader content",
				"error", err,
			)
		}
	case bodyTypeJson:

		if r.body == nil && !canMarshalToJSON(r.body) {

			if !r.NoAutomaticContentLengthHeader && header.Get(HeaderKeyContentLength) == "" {
				header.Set(HeaderKeyContentLength, contentLengthOfNullJsonBody)
			}

			w.WriteHeader(r.StatusCode)

			if _, err := w.Write(nullJsonBody); err != nil {
				if ctx.Err() == context.Canceled {
					return
				}

				r.getLogger(ctx).Debugw(
					"client failed to read all buffered json content",
					"error", err,
				)
			}

			return
		}

		var buf bytes.Buffer
		encoder := json.NewEncoder(&buf)
		encoder.SetEscapeHTML(false)

		if err := encoder.Encode(r.body); err != nil {
			r.writeRenderError(ctx, w, err)
			return
		}

		bytes := buf.Bytes()

		header.Set(HeaderKeyContentType, HeaderValueContentTypeJson)

		if !r.NoAutomaticContentLengthHeader && header.Get(HeaderKeyContentLength) == "" {
			header.Set(HeaderKeyContentLength, strconv.Itoa(len(bytes)))
		}

		w.WriteHeader(r.StatusCode)

		_, err = w.Write(bytes)
		if err != nil {
			if ctx.Err() == context.Canceled {
				return
			}

			r.getLogger(ctx).Debugw(
				"client failed to read all buffered json content",
				"error", err,
			)
		}
	case bodyTypeNone:

		fallthrough
	default:

		if !r.NoAutomaticContentLengthHeader && header.Get(HeaderKeyContentLength) == "" {
			header.Set(HeaderKeyContentLength, "0")
		}

		w.WriteHeader(r.StatusCode)
		return
	}
}

// writeRenderError should only be called before w.WriteHeader() is called
func (r Response) writeRenderError(ctx context.Context, w http.ResponseWriter, err error) {
	var isJsonResponse bool

	// clear any headers that could have already been staged
	{
		header := w.Header()

		isJsonResponse = (header.Get(HeaderKeyContentType) == HeaderValueContentTypeJson)

		for k := range header {
			delete(header, k)
		}
	}

	// protects against really bad rendering cycles
	// in case a code author does wonky stuff
	if r.StatusCode == http.StatusInternalServerError {

		logger := r.getLogger(ctx)

		if isJsonResponse {
			h := w.Header()
			h.Add(HeaderKeyContentType, HeaderValueContentTypeJson)
			h.Add(HeaderKeyContentLength, fallbackErrJsonResponseContentLength)

			w.WriteHeader(http.StatusInternalServerError)

			if _, err := w.Write(fallbackErrJsonResponse); err != nil {
				logger.Debugw(
					"client failed to read all []byte content from fallbackErrJsonResponse",
					"error", err,
				)
			}
		} else {
			// failing to render body, but should set status code
			// at the very least
			w.WriteHeader(http.StatusInternalServerError)
		}

		logger.Errorw(
			ErrCodeInternalServerError,
			"error", err,
		)

		return
	}

	errResp := r.responseFactory().NewInternalErrResponse(err).Response()

	// ensuring status code is 500 to prevent bad rendering cycles
	// in case a code author does wonky stuff
	errResp.StatusCode = http.StatusInternalServerError

	if r.LoggerDisabled {
		errResp.LoggerDisabled = true
	} else {
		errResp.Logger = r.getLogger(ctx)
	}

	errResp.WriteResponse(ctx, w)
}

// HandlerFunc is a helper function that converts
// a response into a http.HandlerFunc
func (r Response) HandlerFunc() http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		r.WriteResponse(req.Context(), w)
	}
}

// StaticHandlerFunc will convert the response body to bytes
// and return a simple http.HandlerFunc
//
// If anything goes wrong converting the body to bytes
// then the function panics
//
// Useful when creating a static response handler
// and all the body fits into memory
func (r Response) StaticHandlerFunc() http.HandlerFunc {
	var bodyBytes []byte

	// zero out pointer members that are not allowed to be defined
	// in this copy of the response object
	r.Logger = nil

	// clone array backed data members which can be mutated
	// by this function
	r.Header = r.Header.Clone()

	switch r.bodyType {
	case bodyTypeJson:

		if r.body == nil && !canMarshalToJSON(r.body) {
			bodyBytes = nullJsonBody

			if r.Header == nil {
				r.Header = http.Header{}
			}
			r.Header.Set(HeaderKeyContentType, HeaderValueContentTypeJson)

			r.bodyType = bodyTypeBytes
			r.body = bodyBytes

			break
		}

		var buf bytes.Buffer
		encoder := json.NewEncoder(&buf)
		encoder.SetEscapeHTML(false)

		if err := encoder.Encode(r.body); err != nil {
			zap.S().Panicw(
				"failed to json marshal http response to create HandlerFunc",
				"error", err,
				"error.stack", string(debug.Stack()),
			)
		}

		bodyBytes = buf.Bytes()

		if r.Header == nil {
			r.Header = http.Header{}
		}
		r.Header.Set(HeaderKeyContentType, HeaderValueContentTypeJson)

		r.bodyType = bodyTypeBytes
		r.body = bodyBytes
	case bodyTypeReader:
		var err error

		reader, ok := r.body.(io.Reader)
		if !ok {
			break
		}

		bodyBytes, err = ioutil.ReadAll(reader)
		if err != nil {
			zap.S().Panicw(
				"failed to read all bytes from io.Reader type http response to create HandlerFunc",
				"error", err,
				"error.stack", string(debug.Stack()),
			)
		}

		r.bodyType = bodyTypeBytes
		r.body = bodyBytes
	case bodyTypeBytes:

		if bytes, ok := r.body.([]byte); ok {
			bodyBytes = bytes
		}
	case bodyTypeNone:
		fallthrough
	default:
		break
	}

	if !r.NoAutomaticContentLengthHeader {

		if r.Header == nil {
			r.Header = http.Header{}
		}

		if r.Header.Get(HeaderKeyContentLength) == "" {
			r.Header.Set(HeaderKeyContentLength, strconv.Itoa(len(bodyBytes)))
		}

		r.NoAutomaticContentLengthHeader = true
	}

	return r.HandlerFunc()
}

func (r Response) responseFactory() *responseFactory {

	if r.respFactory != nil {
		return r.respFactory
	}

	return GetDefaultResponseFactory()
}

func (r Response) logErrorResponse(ctx context.Context) {

	logger := r.getLogger(ctx)

	var errCode, developerMessage string
	var internalErr error

	{
		errCode = r.errResponse.ErrCode
		developerMessage = r.errResponse.DeveloperMessage
		internalErr = r.errResponse.InternalError
	}

	if h := r.ErrorResponseLogHandler; h != nil {
		h(ctx, logger, r.StatusCode, errCode, developerMessage, internalErr)
		return
	}

	rf := r.responseFactory()
	if h := rf.errorResponseLogHandler; h != nil {
		h(ctx, logger, r.StatusCode, errCode, developerMessage, internalErr)
	}
}

func (r Response) getLogger(ctx context.Context) *zap.SugaredLogger {

	if r.LoggerDisabled {
		return zap.NewNop().Sugar()
	}

	if r.Logger != nil {
		return r.Logger
	}

	if r.ContextLogger != nil {
		return r.ContextLogger(ctx)
	}

	rf := r.responseFactory()
	if h := rf.contextLogger; h != nil {
		return h(ctx)
	}

	return zap.NewNop().Sugar()
}
