package response

type loggableErrResponse struct {
	InternalError    error
	StatusCode       int
	ErrCode          string
	DeveloperMessage string
}
