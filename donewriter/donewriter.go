package donewriter

import (
	"context"
)

type mw struct{}

func (mw) Done() bool {
	return true
}

func (m mw) Get(ctx context.Context) Doner {
	return m
}

var Middleware mw

type Doner interface {
	Done() bool
}
