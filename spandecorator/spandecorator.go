package spandecorator

import (
	"errors"
	"net/http"

	"bitbucket.org/phantommachine/go-kit/logging"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
)

const (
	SpanPrefixQueryString           = "http_url_details.queryString."
	SpanTagHttpUseragent            = "http_useragent"
	SpanTagHttpPrivateRemoteAddress = "http_private_remote_address"
)

func DefaultDecorator(span trace.Span, untraceableQueryParams map[string]struct{}, r *http.Request) {
	for k, v := range r.URL.Query() {

		if untraceableQueryParams != nil {
			if _, ok := untraceableQueryParams[k]; ok {
				span.SetAttributes(attribute.String(SpanPrefixQueryString+k, "REDACTED"))
				continue
			}
		}

		span.SetAttributes(attribute.StringSlice(SpanPrefixQueryString+k, v))
	}

	span.SetAttributes(attribute.String(SpanTagHttpUseragent, r.UserAgent()))

	span.SetAttributes(attribute.String(SpanTagHttpPrivateRemoteAddress, logging.GetPrivateRemoteAddress(r)))
}

func DecorateSpan(options ...option) func(http.HandlerFunc) http.HandlerFunc {
	cfg := config{
		untraceableQueryParams: map[string]struct{}{
			"password": {},
		},
		decorator: DefaultDecorator,
	}

	for _, op := range options {
		op(&cfg)
	}

	return func(next http.HandlerFunc) http.HandlerFunc {
		return func(w http.ResponseWriter, r *http.Request) {
			span := trace.SpanFromContext(r.Context())
			cfg.decorator(span, cfg.untraceableQueryParams, r)

			next(w, r)
		}
	}
}

type option func(cfg *config)

type DecoratorFunc func(trace.Span, map[string]struct{}, *http.Request)

type config struct {
	untraceableQueryParamsUpdated bool
	untraceableQueryParams        map[string]struct{}
	decorator                     DecoratorFunc
}

// If you are seriously trying to disable this decorator middleware then just don't use it!
var ErrNilDecoratorFunc = errors.New("middleware-spandecorator: Decorator strategy func cannot be nil")

// UntraceableQueryParams alters the list of query args values to redact.
//
// Their values are replaced with the string REDACTED.
func UntraceableQueryParams(queryStrings ...string) option {
	return func(cfg *config) {

		// if this is the first time setting untraceable query params
		// then clear out the deny-list so the user can have full control
		if !cfg.untraceableQueryParamsUpdated {
			cfg.untraceableQueryParamsUpdated = true

			for k := range cfg.untraceableQueryParams {
				delete(cfg.untraceableQueryParams, k)
			}
		}

		for _, s := range queryStrings {
			cfg.untraceableQueryParams[s] = struct{}{}
		}
	}
}

// Decorator replaces the strategy for adding tags to the span.
//
// panics if a nil DecoratorFunc is supplied
func Decorator(f DecoratorFunc) option {
	return func(cfg *config) {

		if f == nil {
			panic(ErrNilDecoratorFunc)
		}

		cfg.decorator = f
	}
}
