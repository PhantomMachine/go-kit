package traces

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/go-kit/log"
	"go.opentelemetry.io/contrib/instrumentation/net/http/otelhttp"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/exporters/otlp/otlptrace"
	"go.opentelemetry.io/otel/exporters/otlp/otlptrace/otlptracehttp"
	"go.opentelemetry.io/otel/metric"
	"go.opentelemetry.io/otel/sdk/resource"
	sdktrace "go.opentelemetry.io/otel/sdk/trace"
	semconv "go.opentelemetry.io/otel/semconv/v1.17.0"
	"go.opentelemetry.io/otel/trace"
)

func newExporter(ctx context.Context, endpoint string) *otlptrace.Exporter {
	exp, err := otlptracehttp.New(ctx, otlptracehttp.WithEndpoint(endpoint), otlptracehttp.WithInsecure())
	if err != nil {
		panic(err)
	}

	return exp
}

func newTraceProvider(serviceName string, exp *otlptrace.Exporter) *sdktrace.TracerProvider {
	res, err := resource.Merge(
		resource.Default(),
		resource.NewWithAttributes(
			semconv.SchemaURL,
			// the service name used to display traces in backends
			semconv.ServiceName(serviceName),
			attribute.String("library.language", "go"),
		),
	)
	handleErr(err, "failed to create resource")

	return sdktrace.NewTracerProvider(
		sdktrace.WithBatcher(exp),
		sdktrace.WithResource(res),
	)
}

func handleErr(err error, message string) {
	if err != nil {
		panic(fmt.Sprintf("%s: %s", err, message))
	}
}

func Init(ctx context.Context, serviceName, endpoint string) func() {
	var exp *otlptrace.Exporter
	if endpoint != "" {
		exp = newExporter(ctx, endpoint)
	}

	tp := newTraceProvider(serviceName, exp)

	// set global propagator to tracecontext (the default is no-op).
	// otel.SetTextMapPropagator(propagation.TraceContext{})
	otel.SetTracerProvider(tp)

	return func() {
		// Shutdown will flush any remaining spans.
		handleErr(tp.Shutdown(ctx), "failed to shutdown TracerProvider")
	}
}

func Tracer(name string) StartTracer {
	return otel.Tracer(name)
}

func Stop() {
}

func Client() http.Client {
	return http.Client{Transport: otelhttp.NewTransport(http.DefaultTransport)}
}

func Logger() log.Logger {
	logger := log.NewLogfmtLogger(log.NewSyncWriter(os.Stdout))
	return log.With(logger, "ts", log.DefaultTimestampUTC)
}

func SpanContextFromContext(ctx context.Context) trace.SpanContext {
	return trace.SpanContextFromContext(ctx)
}

func GetTraceID(ctx context.Context) string {
	return trace.SpanContextFromContext(ctx).TraceID().String()
}

type StatusRecorder struct {
	http.ResponseWriter
	Status int
}

func (sr *StatusRecorder) WriteHeader(statusCode int) {
	sr.Status = statusCode
	sr.ResponseWriter.WriteHeader(statusCode)
}

func getMeter() metric.Meter {
	return otel.GetMeterProvider().Meter("bitbucket.org/phantommachine/traces")
}

// TODO: move new handler into appropriate package when created, ie custom router or server packages
func NewHandler(logger log.Logger, next http.HandlerFunc) http.HandlerFunc {
	// OpenMetrics handler : metrics and exemplars
	omHandleFunc := func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()

		sr := &StatusRecorder{w, 200}

		next.ServeHTTP(sr, r)

		ctx := r.Context()
		traceID := GetTraceID(ctx) // trace.SpanContextFromContext(ctx).TraceID.String()

		// log the trace id with other fields so we can discover traces through logs
		logger.Log("msg", "http request", "traceID", traceID, "method", r.Method, "path", r.URL.Path, "latency", time.Since(start), "response_code", sr.Status)

		// TODO: figure out some better way of naming these metrics
		pathName := strings.Replace(r.URL.Path, "/", "_", -1)
		pathName = fmt.Sprintf("%s%s", strings.ToLower(r.Method), pathName)

		counter, _ := getMeter().Int64Counter(fmt.Sprintf("http_%s_count", pathName))
		counter.Add(r.Context(), 1, metric.WithAttributes(
			attribute.Int("status_code", sr.Status),
		))
	}
	return otelhttp.NewHandler(http.HandlerFunc(omHandleFunc), "http").ServeHTTP
}

type StartTracer interface {
	Start(ctx context.Context, spanName string, opts ...trace.SpanStartOption) (context.Context, trace.Span)
}
