package middlewarechain

import "net/http"

type Link = func(http.HandlerFunc) http.HandlerFunc
type Chain []Link

func New(links ...Link) Chain {
	return Chain(links).Copy()
}

func (c Chain) Copy() Chain {
	newCopy := make(Chain, len(c))
	copy(newCopy, c)
	return newCopy
}

func (c Chain) HandlerFunc(handle http.HandlerFunc) http.HandlerFunc {
	for i := len(c) - 1; i >= 0; i-- {
		handle = c[i](handle)
	}

	return handle
}
