package requestdecoder

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"mime"
	"net/http"
	"reflect"
	"strings"

	"bitbucket.org/phantommachine/go-kit/response"
	"github.com/gorilla/schema"
	"github.com/julienschmidt/httprouter"
)

type RequestDecoder struct {
	*schema.Decoder
}

func New() *RequestDecoder {
	decoder := schema.NewDecoder()
	decoder.IgnoreUnknownKeys(true)
	decoder.SetAliasTag("json")

	return &RequestDecoder{
		Decoder: decoder,
	}
}

var Decoder *RequestDecoder

func init() {
	Decoder = New()
}

func Decode(v interface{}, p map[string][]string) error {
	return Decoder.Decode(v, p)
}

func DecodeHeader(r *http.Request, v interface{}) error {
	return Decoder.DecodeHeader(r, v)
}

func (reqDecoder *RequestDecoder) DecodeFrom(prefix string, v interface{}, p map[string][]string) error {
	structPtrVal := reflect.ValueOf(v)

	if structPtrVal.Kind() != reflect.Ptr || structPtrVal.IsNil() {
		return nil
	}

	structVal := structPtrVal.Elem()
	structType := structVal.Type()

	if structType.Kind() != reflect.Struct {
		return nil
	}

	startIdx := -1
	numFields := structVal.NumField()
	for i := 0; startIdx < 0 && i < numFields; i++ {
		fieldVal := structVal.Field(i)

		if !fieldVal.CanSet() {
			continue
		}

		fieldName := structType.Field(i).Name

		if !strings.HasPrefix(fieldName, prefix) {
			continue
		}

		startIdx = i
	}

	if startIdx < 0 {
		return nil
	}

	structCopyPtrVal := reflect.New(structType)

	if err := reqDecoder.Decode(structCopyPtrVal.Interface(), p); err != nil {
		return err
	}

	structCopyVal := structCopyPtrVal.Elem()

	structVal.Field(startIdx).Set(structCopyVal.Field(startIdx))
	startIdx++

	for i := startIdx; i < numFields; i++ {
		fieldVal := structVal.Field(i)

		if !fieldVal.CanSet() {
			continue
		}

		fieldName := structType.Field(i).Name

		if !strings.HasPrefix(fieldName, prefix) {
			continue
		}

		structVal.Field(i).Set(structCopyVal.Field(i))
	}

	return nil
}

func (reqDecoder *RequestDecoder) DecodeHeader(r *http.Request, v interface{}) error {
	return reqDecoder.DecodeFrom("Header", v, r.Header)
}

func DecodeQuery(r *http.Request, v interface{}) error {
	return Decoder.DecodeQuery(r, v)
}

func (reqDecoder *RequestDecoder) DecodeQuery(r *http.Request, v interface{}) error {
	return reqDecoder.DecodeFrom("Query", v, r.URL.Query())
}

func DecodePath(r *http.Request, v interface{}) error {
	return Decoder.DecodePath(r, v)
}

func (reqDecoder *RequestDecoder) DecodePath(r *http.Request, v interface{}) error {
	if v == nil {
		return nil
	}

	paramMap := make(map[string][]string)
	params := httprouter.ParamsFromContext(r.Context())
	for _, p := range params {
		paramMap[p.Key] = []string{p.Value}
	}

	return reqDecoder.DecodeFrom("Path", v, paramMap)
}

func DecodeBody(r *http.Request, v interface{}) error {
	return Decoder.DecodeBody(r, v)
}

func (reqDecoder *RequestDecoder) DecodeBody(r *http.Request, v interface{}) error {
	if r.ContentLength == 0 {
		return nil
	}

	val := reflect.ValueOf(v)
	if val.Kind() != reflect.Ptr {
		return nil
	}

	body := val.Elem().FieldByName("Body")

	if !body.IsValid() || !body.CanSet() {
		return nil
	}

	var contentType string
	if rawContentType := r.Header.Get(response.HeaderKeyContentType); rawContentType != "" {
		var err error
		contentType, _, err = mime.ParseMediaType(rawContentType)
		if err != nil {
			return err
		}
	}

	switch contentType {
	case response.HeaderValueContentTypeJson:

		b := reflect.New(body.Type()).Interface()
		bodyReader := r.Body
		var bodyBytes bytes.Buffer

		_, err := io.Copy(&bodyBytes, bodyReader)
		if err != nil {
			return err
		}
		defer bodyReader.Close()

		r.Body = io.NopCloser(&bodyBytes)
		if err := json.NewDecoder(&bodyBytes).Decode(b); err != nil {
			return err
		}

		body.Set(reflect.ValueOf(b).Elem())
	default:
		msg := fmt.Sprintf("unknown content type: %s", contentType)
		return response.NewErrResponse(http.StatusBadRequest, http.StatusText(http.StatusBadRequest), msg)
	}

	return nil
}

func DecodeRequest(r *http.Request, v interface{}) error {
	return Decoder.DecodeRequest(r, v)
}

func (reqDecoder *RequestDecoder) DecodeRequest(r *http.Request, v interface{}) error {
	if err := reqDecoder.DecodeHeader(r, v); err != nil {
		return fmt.Errorf("request decode failed: %w", err)
	}

	if err := reqDecoder.DecodeQuery(r, v); err != nil {
		// err can be an error response, so just return it
		return fmt.Errorf("request query decode failed: %w", err)
	}
	if err := reqDecoder.DecodePath(r, v); err != nil {
		return fmt.Errorf("request path decode failed: %w", err)
	}
	if err := reqDecoder.DecodeBody(r, v); err != nil {
		return fmt.Errorf("request body decode failed: %w", err)
	}

	return nil
}
